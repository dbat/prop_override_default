@tool
class_name B
extends A

## Not the best, but all I could manage.
## You at least get a category that says "B" with the "Avar" property in it.

# My default
const B_DEFAULT = preload("res://alternative_texture2d.tres")

# Must be here to get this hack to work.
func _get_property_list() -> Array[Dictionary]:
	return [
		{ 
		# I got these values from the debug 'tog' in "A"
		"name": &"/avar", # the slash is a hack I got from https://github.com/godotengine/godot/issues/56614
		"class_name": &"Texture2D", 
		"type": 24, 
		"hint": 17, 
		"hint_string": "Texture2D", 
		"usage": 4102,
		# Tried this one \/
		# "usage": PROPERTY_USAGE_NO_EDITOR
		# But it causes the whole thing to break
		}
	]

# Without the _get_property_list func this does not fire at all.
func _set(property, value) -> bool:
	print("set ", property, " to ", value)
	match property:
		&"/avar": # the slash is a hack I got from https://github.com/godotengine/godot/issues/56614
			avar = value
			notify_property_list_changed()
	return false

# Same story. This is where we enforce the default for "B"
func _get(property):
	var value = null
	match property:
		&"/avar":
			if avar == A_DEFAULT:
				value = B_DEFAULT
			else:
				value = avar

	return value
